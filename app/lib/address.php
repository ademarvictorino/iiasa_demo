<?php


require_once('database.php');

/*
 *
 * Address object to be handle and persisted
 *
 */

class Address{

    /*
     *
     * ValueObjects atributes
     *
     */
    private $email = "";
    private $address = "";
    private $lat = "";
    private $lng = "";
    private $phone = "";
    private $job = "";
    private $id = "";

    function __construct() {

    }

    /**
    * Function get
    *
    * Retrieve one object from the storage
    *
    * @param (int) primary key
    * @return (object) object that represent the persisted class
    */
    function get($id){
        $o = new MongoDBStorage('address');
        return $this->from_obj($o->get($id));
    }

    /**
    * Static function load
    * Retrieve one object from the storage.
    * Similar to get but using the static way
    *
    * @param (int) primary key
    * @return (object) object that represent the persisted class
    */
    static function load($id){
        $addr = new Address();
        $addr->get($id);
        return $addr;
    }

    /**
    * Static function all
    *
    * @return (array) array of objects
    */
    static function all(){
        $o = new MongoDBStorage('address');
        return Address::from_objs($o->all());
    }

    /**
    * Function delete
    *
    * @param (int) primary key
    */
    function delete(){
        $o = new MongoDBStorage('address');
        $o->delete($this->getId());
    }

    /**
    * Function save
    *
    * @return (object) return the same object, but if it was created the value pk is added in your body
    */
    function save(){
        $o = new MongoDBStorage('address');
        $obj = $o->save($this->to_obj());
        $this->setId($obj['id']);
        return $this;
    }

    /**
     * Function to_obj
     * Tranform the class attributes in a key array
     *
     * @return (object) return the same object, but if it was created the value pk is added in your body
     */
    public function to_obj(){
        $obj = array(
            "email" => $this->getEmail(),
            "address" => $this->getAddress(),
            "lat" => $this->getLat(),
            "lng" => $this->getLng(),
            "phone" => $this->getPhone(),
            "job" => $this->getJob(),
            "id" => $this->getId()
        );
        return $obj;
    }

    /**
     * Static function to_array
     * Transform an array of class objects in array of dictionaries
     *
     * @return (array) object of dictionaries
     */
    static function to_array($address_array){
        $n_array = [];
        foreach ($address_array as $obj) {
            array_push($n_array, $obj->to_obj());
        }
        return $n_array;
    }

    /**
     * private function from_obj
     * Fill the array keys objects in class attributes
     *
     * @param (array) Key Array
     */
    private function from_obj($obj){

        $this->setEmail($obj['email']);
        $this->setAddress($obj['address']);
        $this->setLat($obj['lat']);
        $this->setLng($obj['lng']);
        $this->setPhone($obj['phone']);
        $this->setJob($obj['job']);
        $this->setId((string)$obj['_id']);

    }

    /**
     * private function from_objs
     * Fill the arrays keys objects in class attributes
     *
     * @param (array) Key Arrays
     */
    private static function from_objs($array){

        $n_array = [];
        foreach ($array as $obj) {
            $addr = new Address();
            $addr->setEmail($obj['email']);
            $addr->setAddress($obj['address']);
            $addr->setLat($obj['lat']);
            $addr->setLng($obj['lng']);
            $addr->setPhone($obj['phone']);
            $addr->setJob($obj['job']);
            $addr->setId((string)$obj['_id']);
            array_push($n_array, $addr);
        }
        return $n_array;
    }

    /**
     * Static function fake
     * Create fake initial fake data
     */
    static function fake(){

        $address = new Address();
        $address->address = "IZ Niederösterreich Süd Straße";
        $address->lat = 48.062304;
        $address->lng = 16.331127;
        $address->email = "fake@gmail.com";
        $address->save();

        $address = new Address();
        $address->address = "Natterergasse 3";
        $address->lat = 48.073929;
        $address->lng = 16.351935;
        $address->email = "fake@gmail.com";
        $address->save();

        $address = new Address();
        $address->address = "IZ Niederösterreich Süd Straße 7 Obj";
        $address->lat = 48.065902;
        $address->lng = 16.336301;
        $address->email = "fake@gmail.com";
        $address->save();


    }

    /**
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $institution
     */
    public function setInstitution($institution)
    {
        $this->institution = $institution;
    }

    /**
     * @return string
     */
    public function getInstitution()
    {
        return $this->institution;
    }

    /**
     * @param string $job
     */
    public function setJob($job)
    {
        $this->job = $job;
    }

    /**
     * @return string
     */
    public function getJob()
    {
        return $this->job;
    }

    /**
     * @param string $lat
     */
    public function setLat($lat)
    {
        $this->lat = $lat;
    }

    /**
     * @return string
     */
    public function getLat()
    {
        return $this->lat;
    }

    /**
     * @param string $lng
     */
    public function setLng($lng)
    {
        $this->lng = $lng;
    }

    /**
     * @return string
     */
    public function getLng()
    {
        return $this->lng;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param string $pk
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getId(){
        return $this->id;
    }

    function __destruct() {
        //
    }
}
?>