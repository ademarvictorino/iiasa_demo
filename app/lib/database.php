<?php

    /*
     *
     * This is an simple abstraction os an ORM framework, as not mandatory store the data in a database I did a simulation
     * of how a database access framework works using an agnostic and nosql storage.
     *
     */
    class MongoDBStorage{

        private $class_name = null;
        private $collection = null;

        /*
         *
         * Instantiate MongoDb library and create a new table/document if not exist,
         * This class should be used to simple write/read operations in a table.
         *
         * @param (string) class name, creates a new table/document if not exists
         */

        function __construct($class_name) {
            $this->class_name = $class_name;
            $m = new MongoClient();
            $dbm = $m->selectDB("iiasa_demo");
            $dbm->createCollection($class_name);
            $this->collection = $dbm->selectCollection($class_name);
        }

        /*
        * Function get
        *
        * Retrieve one object from the storage
        *
        * @param (int) primary key
        * @return (object) object that represent the persisted class
        */
        function get($_id){
            $obj = $this->collection->findOne(array('_id' => new MongoId($_id)));
            $obj['id'] = (string)$obj['_id'];
            return $obj;
        }

        /*
        * Function filter
        *
        * Retrieve many objects from the storage
        *
        * @param (array) primary keys array [10, 11, 12]
        * @return (object) object that represent the persisted class
        */
        function filter($pk_array){
            //TODO: When I'll need I'll write
        }

        /*
        * Function delete
        *
        * remove one object from the storage
        *
        * @param (int) primary key
        */
        function delete($id){
            $this->collection->remove(array('_id' => new MongoId($id)));
        }

        /*
        * Function save
        *
        * Insert or update the object in the storage, if has a pk the function takes this id and update the all others values,
        * if the id not exists in the object will be create a new entry
        *
        * @param (object) object that represent the persisted class
        * @return (object) return the same object, but if it was created the value pk is added in your body
        */
        function save($obj){

            if(array_key_exists("id", $obj) && $obj['id'] != ""){
                $this->collection->update(array('_id' => $obj['id']), $obj);
            }else{
                $this->collection->insert($obj);
                $obj['id'] = (string)$obj['_id'];
            }
            return $obj;

        }

        /*
        * Function all
        *
        * Retrieve all models of same instance from the storage
        *
        * @return (array) array of objects
        */
        function all(){
            return iterator_to_array($this->collection->find());
        }

        function __destruct() {

        }
    }
?>