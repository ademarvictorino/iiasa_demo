<?php


    /*
     *
     * Convert and save the address in the database
     *
     */

    require_once("../lib/address.php");

    $postdata = file_get_contents("php://input");
    $request = json_decode($postdata);

    $address = new Address();
    if($request->id != ""){
        $address->get($request->id);
    }
    $address->setLat($request->lat);
    $address->setLng($request->lng);
    $address->setAddress($request->address);
    $address->setJob($request->job);
    $address->setPhone($request->phone);
    $address->setEmail($request->email);

    $address->save();

    echo json_encode($address->to_obj());

?>