<?php

    /*
     *
     * Delete an address by ID
     *
     */
    header('Content-type:application/json;charset=utf-8');
    require_once("../lib/address.php");

    $postdata = file_get_contents("php://input");
    $request = json_decode($postdata);

    $addrees = Address::load($request->addr_id);
    $addrees->delete();
    echo json_encode(array("status" => "ok"));
    exit(0);

?>