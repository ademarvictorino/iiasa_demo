<?php
    /*
     *
     * Export json with all address in the database.
     *
     */
    header('Content-type:application/json;charset=utf-8');

    require_once("../lib/address.php");

    $address_array = Address::all();

    if(count($address_array) == 0){
        Address::fake();
        $address_array = Address::all();
    }

    echo json_encode(Address::to_array($address_array));

    exit(0);

?>