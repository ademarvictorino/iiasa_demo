from fabric.api import *
from fabric.contrib.project import *
import os
env.shell = '/bin/bash -c'


def deploy_aws():

    env.host_string = '52.36.79.166'
    env.user = 'ubuntu'
    env.key_filename = "/Users/victorino/Documents/gigoia_docs/expresso_ligeirinho.pem"

    with cd("/opt/iiasa_demo"):
        sudo("git reset --hard")
        sudo("git pull")

    sudo("cp /opt/iiasa_demo/iiasa.conf /etc/apache2/sites-enabled/iiasa.conf")
    sudo("cp /opt/iiasa_demo/iiasa-le-ssl.conf /etc/apache2/sites-enabled/iiasa-le-ssl.conf")

    sudo("service apache2 reload")
