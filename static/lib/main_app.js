/*

Angular controller to manipulate the html DOM, do complex algorithm, and store models in server side.
Underscore library to help the queries in iterations inside arrays.

*/

angular.module('indexApp', ['ngAnimate']).controller('indexController', function($scope, $http) {


    /*

    Angular simple attributes to control the HTML DOM

    */
    $scope.show_hide_click_area_text = "Hide clickable area";
    $scope.show_hide_austria_polygon_text = "Show Austria layer"
    $scope.server_url = "";
    $scope.main_error_mensage = "";
    $scope.display_leaflet_map = false;
    $scope.address = [];
    $scope.form_address = "";
    $scope.form_email = "";
    $scope.form_job = "";
    $scope.form_phone = "";
    $scope.form_lat = "";
    $scope.form_lng = "";
    $scope.form_id = "";
    $scope.form_marker = null;
    $scope.geocoder = null;
    $scope.xdebug_pid = "11811";
    $scope.leaflet_world_layer = null;
    /*

    Complex attributes

    */
    $scope.default_center_point = new google.maps.LatLng(48.068252, 16.358373);
    $scope.map_leaflet = L.map('map_leaflet').setView([51.505, -0.09], 13);
    $scope.map = new google.maps.Map(document.getElementById('map'), {
        scrollwheel: true,
        zoom: 12,
        zoomControl: true,
        mapTypeControl: false,
        streetViewControl: false,
        disableDoubleClickZoom: true,
        draggable: true,
        styles: [{"stylers":[{"hue":"#bbff00"},{"weight":0.5},{"gamma":0.5}]},{"elementType":"labels","stylers":[{"visibility":"off"}]},{"featureType":"landscape.natural","stylers":[{"color":"#a4cc48"}]},{"featureType":"road","elementType":"geometry","stylers":[{"color":"#ffffff"},{"visibility":"on"},{"weight":1}]},{"featureType":"administrative","elementType":"labels","stylers":[{"visibility":"on"}]},{"featureType":"road.highway","elementType":"labels","stylers":[{"visibility":"simplified"},{"gamma":1.14},{"saturation":-18}]},{"featureType":"road.highway.controlled_access","elementType":"labels","stylers":[{"saturation":30},{"gamma":0.76}]},{"featureType":"road.local","stylers":[{"visibility":"simplified"},{"weight":0.4},{"lightness":-8}]},{"featureType":"water","stylers":[{"color":"#4aaecc"}]},{"featureType":"landscape.man_made","stylers":[{"color":"#718e32"}]},{"featureType":"poi.business","stylers":[{"saturation":68},{"lightness":-61}]},{"featureType":"administrative.locality","elementType": "labels.text.stroke","stylers":[{"weight":2.7},{"color":"#f4f9e8"}]},{"featureType":"road.highway.controlled_access","elementType":"geometry.stroke","stylers":[{"weight":1.5},{"color":"#e53013"},{"saturation":-42},{"lightness":28}]}]
    });
    $scope.paths = [
        {lat: 48.097598, lng: 16.281386, name:"Maria Enzersdorf"},
        {lat: 48.099687, lng: 16.421763, name:"Europastraße, Maria Lanzendorf"},
        {lat: 48.037939, lng: 16.378476, name:"Wienerstraße, Münchendorf"},
        {lat: 48.040757, lng: 16.280728, name:"Gartengasse"}
        ];
    $scope.limits_boundary_polygon = new google.maps.Polygon({
        paths: $scope.paths,
        strokeColor: '#FF0000',
        strokeOpacity: 0.5,
        strokeWeight: 1,
        fillColor: '#bfbebe',
        fillOpacity: 0.5
    });

    /*

    Init the page and load the initial data from server

    */
    $scope.init = function(){

        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(function(position) {

                try{
                    $scope.init_google_map(new google.maps.LatLng(position.coords.latitude, position.coords.longitude));
                    $scope.init_leaflet_map();
                }catch(err){
                    $scope.init_google_map(null);
                    $scope.init_leaflet_map();
                }

            });

        }else{
            $scope.init_google_map(null);
            $scope.init_leaflet_map();
        }

        $http({
            url: $scope.server_url = "/app/public/load_address.php?XDEBUG_SESSION_START=" + $scope.xdebug_pid,
            method: "POST"
        }).success(function(data){

            $scope.address = data;
            setTimeout(function(){
                $scope.draw_markers()
            }, 2000);

        });

    };

    /*
    Init the map data, callbacks and shapes
     */
    $scope.init_google_map = function(position){

        if(google.maps.geometry != undefined && google.maps.geometry.poly.containsLocation(position, $scope.limits_boundary_polygon)){
            $scope.map.setCenter(position);
        }else{
            $scope.fit_iiasa();
        }

        $scope.map.data.loadGeoJson('austria.geojson');

        $scope.map.data.setStyle({
          fillColor: 'green',
          strokeWeight: 0.5,
          visible: false
        });

        $scope.limits_boundary_polygon.setMap($scope.map);

        

        $scope.geocoder = new google.maps.Geocoder;

        $scope.map.addListener('click', function(e) {
            $scope.click_map(e.latLng);
        });

        $scope.map.data.addListener('click', function(e) {
            $scope.click_map(e.latLng);
        });

        $scope.limits_boundary_polygon.addListener('click', function(e) {
            $scope.click_map(e.latLng);
        });

    };

    /*

    Call server side method to delete an address from database

     */
    $scope.delete_address = function(addr){

        $http({
            url: $scope.server_url = "/app/public/delete_address.php?XDEBUG_SESSION_START=" + $scope.xdebug_pid,
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: {addr_id: addr.id}
        }).success(function(data){

            var deleted_addr = _.find($scope.address, {id: addr.id});
            if(deleted_addr != null && deleted_addr.hasOwnProperty("marker")){
                if(deleted_addr.marker != undefined){
                    deleted_addr.marker.setMap(null);
                    delete deleted_addr["marker"];
                }
            }
            $scope.address = _.without($scope.address, _.findWhere($scope.address, {id: addr.id}))

        });
    };


    /*

    Draw all markers one time only

    */
    $scope.draw_markers = function(){
        _.each($scope.address, function(addr){

            if(!addr.hasOwnProperty("marker")){
                var marker = new google.maps.Marker({
                    position: new google.maps.LatLng(addr.lat, addr.lng),
                    animation: google.maps.Animation.DROP,
                    map: $scope.map,
                    title: addr.email
                });
                addr.marker = marker;
            }
        });
    };

    /*

    Change the visibility of Austria shape

    */
    $scope.show_austria_area = function(){
        
        if($scope.show_hide_austria_polygon_text.indexOf("Hide") !== -1){
            $scope.map.data.setStyle({
                visible: false,
                fillColor: 'green',
                strokeWeight: 0.5
            }); 
            $scope.show_hide_austria_polygon_text = "Show Austria layer"
            $scope.fit_iiasa();
        }else{
            $scope.map.data.setStyle({
                visible: true,
                fillColor: 'green',
                strokeWeight: 0.5
            }); 
            var bounds = new google.maps.LatLngBounds();
            $scope.map.data.forEach(function(feature) {
                processPoints(feature.getGeometry(), bounds.extend, bounds);
            });
            $scope.map.fitBounds(bounds);
            $scope.show_hide_austria_polygon_text = "Hide Austria layer"
        }
    };

    /*

    Centralize the map aiming for the IIASA center

    */
    $scope.fit_iiasa = function(){

        if($scope.map != null){

            var positions = new google.maps.LatLngBounds();
            _.each($scope.paths, function(path){
                positions.extend(new google.maps.LatLng(path.lat, path.lng));
            });
            $scope.map.setZoom(12);
            $scope.map.fitBounds(positions);
        }

    };

    $scope.init_leaflet_map = function(){

        L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
            attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        }).addTo($scope.map_leaflet);

        $http({
            url: $scope.server_url = "world.geojson",
            method: "POST"
        }).success(function(data){

            $scope.leaflet_world_layer = data;

            function getColor(d) {
                return d < 6692037.00 ? '#2b83ba' :
                       d < 21627557.00  ? '#80bfac' :
                       d < 47967266.00  ? '#c7e9ad' :
                       d < 104266392.00  ? '#ffffbf' :
                       d < 226063044   ? '#fec980' :
                       d < 1134403141   ? '#f17c4a' :
                       d < 1312978855   ? '#d7191c' :
                       d < 21627557   ? '#d7191c' :
                        '#d7191c';
            }

            function style(feature) {
                return {
                    fillColor: getColor(feature.properties.POP2005),
                    weight: 0.5,
                    opacity: 1,
                    color: 'white',
                    dashArray: '3',
                    fillOpacity: 0.3
                };
            }

            var layer = L.geoJson($scope.leaflet_world_layer, {style: style}).addTo($scope.map_leaflet);

            $scope.map_leaflet.fitBounds(layer.getBounds());

        });
    }

    /*

    Callback sended from map and layers, verify if the point clicked is inside the limited area

     */
    $scope.click_map = function(latLng){

        try{
            if(!google.maps.geometry.poly.containsLocation(latLng, $scope.limits_boundary_polygon)){
                $scope.main_error_mensage = "Please click inside the polygon area.";
                $scope.$apply();
                setTimeout(function(){
                    $scope.main_error_mensage = "";
                    $scope.$apply();
                }, 2000);
                return;
            }else{

                if($scope.form_marker != null){
                    $scope.form_marker.setPosition(latLng);
                }else{
                    $scope.form_marker = new google.maps.Marker({
                        position: latLng,
                        animation: google.maps.Animation.DROP,
                        map: $scope.map,
                        color: "blue",
                    });    
                }

                $scope.geocoder.geocode({'location': latLng}, function(results, status) {

                    if (status === 'OK' && results.length > 0) {
                        $scope.form_address = results[0].formatted_address;
                        $("#id_panel_635198").click();
                        $scope.form_lat = latLng.lat();
                        $scope.form_lng = latLng.lng();
                        $scope.$apply();
                    }
                });
                
                
            }
        }catch(err){
            console.log(err);
        }


    };

    /*

    Receive all iteraction from map or form, move the marker if clicked ou change the address text in the input

    */
    $scope.move_marker = function(){

        if($scope.form_address == null || $scope.form_address == undefined || $scope.form_address == ""){
            return;
        }

        $scope.geocoder.geocode({'address': $scope.form_address}, function(results, status) {

            if (status === 'OK' && results.length > 0) {

                if(google.maps.geometry.poly.containsLocation(results[0].geometry.location, $scope.limits_boundary_polygon)){
                    if($scope.form_marker == null){
                        $scope.form_marker = new google.maps.Marker({
                            position: results[0].geometry.location,
                            animation: google.maps.Animation.DROP,
                            map: $scope.map,
                            color: "blue",
                        });
                    }else{
                        $scope.form_marker.setPosition(results[0].geometry.location);
                    }
                    $scope.form_lat = results[0].geometry.location.lat();
                    $scope.form_lng = results[0].geometry.location.lng(); 
                }else{
                    $scope.main_error_mensage = "Please click inside the polygon area.";
                    $scope.$apply();
                    setTimeout(function(){
                        $scope.main_error_mensage = "";
                        $scope.$apply();
                    }, 2000);
                    return;
                }

            }else{
                $scope.main_error_mensage = "Address '" + $scope.form_address + "' not found.";
                $scope.$apply();
                setTimeout(function(){
                    $scope.main_error_mensage = "";
                    $scope.$apply();
                }, 2000);
            }

        });

    };

    /*

    Helper to the boolean operation

    */
    $scope.disable_save_button = function(){
        if($scope.form_address == "" || ($scope.form_email == undefined || $scope.form_email == "") || $scope.form_lat == "" || $scope.form_lng == ""){
            return true;
        }
        return false;
    }

    /*

    Submit the form to the server side to store in the database

    */
    $scope.save_form = function(){

        $http({
            url: $scope.server_url = "/app/public/save_address.php?XDEBUG_SESSION_START=" + $scope.xdebug_pid,
            method: "POST",
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            data: {
                address: $scope.form_address,
                lat: $scope.form_lat,
                lng: $scope.form_lng,
                job: $scope.form_job,
                phone: $scope.form_phone,
                email: $scope.form_email,
                id: $scope.form_id,
            }
        }).success(function(data){

            if($scope.form_marker != null){
                $scope.form_marker.setMap(null);
            }
            if($scope.form_id == ""){
                $scope.address.push(data);
            }else{
                for(var i = 0; i < $scope.address.length; i++){
                    if($scope.address[i].id == $scope.form_id){
                        $scope.address[i] = data;
                        break
                    }
                }
            }
            
            $scope.reset_form();
            $("#id_panel_510029").click();

        });

    };


    /*

    Receive the event when the user click in a table of address line

    */
    $scope.edit_address = function(addr){

        $scope.form_address = addr.address
        $scope.form_lat = addr.lat
        $scope.form_lng = addr.lng
        $scope.form_job = addr.job
        $scope.form_phone = addr.phone
        $scope.form_email = addr.email;
        $scope.form_id = addr.id;
        $scope.form_marker = addr.marker;

        $("#id_panel_635198").click();

    };

    /*

    Clear the form data

    */
    $scope.reset_form = function(){

        $scope.form_address = "";
        if($scope.form_marker != null){
            $scope.form_marker.setMap(null);
        }
        $scope.form_marker = null;
        $scope.form_lat = "";
        $scope.form_lng = "";
        $scope.form_email = "";
        $scope.form_job = "";
        $scope.form_id = "";
        $scope.form_phone = "";
        $scope.draw_markers();

    }

    /*

    Change the visibility of the clickable area

     */
    $scope.show_clickable_area = function(){
        if($scope.limits_boundary_polygon.map != null){
            $scope.limits_boundary_polygon.setMap(null);
            $scope.show_hide_click_area_text = "Show clickable area";
        }else{
            $scope.limits_boundary_polygon.setMap($scope.map);
            $scope.show_hide_click_area_text = "Hide clickable area";
        }
    }


    /*

    Unify all points of a geojson to claa fitBounds on Austria layer, Thanks Google to help me with this function

    */
    function processPoints(geometry, callback, thisArg) {
        if (geometry instanceof google.maps.LatLng) {
            callback.call(thisArg, geometry);
        } else if (geometry instanceof google.maps.Data.Point) {
            callback.call(thisArg, geometry.get());
        } else {
            geometry.getArray().forEach(function(g) {
                processPoints(g, callback, thisArg);
            });
        }
    };

    $scope.init();


});